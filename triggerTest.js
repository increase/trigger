/*jslint vars: true, sloppy: true, nomen: true */
/*global angular, Case, setup, trigger, syntaxTree, console, LP */
var app = angular.module('testPanel', [

]).run(function ($rootScope) {
	var iframe = document.querySelector('iframe');
	$rootScope.iframe = iframe;
}).controller('panel', function ($scope, $element) {
	var object = {
		button: '[type=button]'
	};

	$scope.actionParam = {
		value: ''
	};
	$scope.opts = {
		selector: '',
		action: ''
	};

	$scope.trigger = null;

	$scope.getActionOfDOM = function () {
		var e = $scope.iframe.contentWindow.document.querySelector($scope.opts.selector);
		$scope.DOMActions = trigger(e).getActionRule();
	};

	$scope.fire = function () {
		var e = $scope.iframe.contentWindow.document.querySelector($scope.opts.selector);
		try {
			$scope.trigger = trigger(e).does($scope.opts.action, $scope.actionParam);
		} catch (err) {
			alert(err.stack);
		}
	};

	$scope.selectors = {
		textInput: 'input',
		emailInput: 'input[type=email]',
		passwordInput: 'input[type=password]',
		textarea: 'textarea',
		buttonInput: 'input[type=button]',
		a: 'a',
		select: 'select'
	};



});
